# TwRaphiBot

Bans all users which have 4 consecutive digits in their nickname.

## Usage

1. Copy `config.def.py` to `config.py`
2. Edit `config.py`
3. `pip install -r requirements.txt`
4. `./bot.py`

## License

Apache 2, see COPYING.txt

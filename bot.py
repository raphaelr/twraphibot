#!/usr/bin/env python3
import pydle
import config
import re

bad_username = re.compile("\\d{4}")

class TwRaphiBot(pydle.Client):
    async def on_connect(self):
        await super().on_connect()
        print("connected.")
        for channel in config.channels:
            await self.join(channel)

    async def on_join(self, channel, user):
        await super().on_join(channel, user)
        if user == self.nickname:
            print("joined", channel)

    async def on_message(self, target, source, message):
        await super().on_message(target, source, message)
        if bad_username.search(source):
            print("in", target, "ban", source)
            await self.message(target, "/ban {}".format(source))
        if message.startswith(config.prefix):
            for trigger, response in config.commands.items():
                if message.startswith(trigger, len(config.prefix)):
                    await self.message(target, response)
    
client = TwRaphiBot(config.nick, realname=config.nick)
client.run("irc.chat.twitch.tv", tls=True, password=config.password)
